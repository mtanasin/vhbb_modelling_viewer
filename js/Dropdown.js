var plotObject = {
    "sherpa_221_vs_2211":["0L", "2L"]
    }
var processObject = ["Zbb", "Zbc", "Zbl", "Zcc", "Zcl", "Zl", "Wbb", "Wbc", "Wbl", "Wcc", "Wcl", "Wl"]
var njetObject = ["2btag2jet", "2btag3jet"]
var ptvObject = ["75_150", "150_250", "250_400"]
var regObject = ["SR"]

window.onload = function() {
    var generatorSel = document.getElementById("generator");
    var channelSel = document.getElementById("channel");
    var processSel = document.getElementById("process");
    var njetSel = document.getElementById("jet_n");
    var ptvSel = document.getElementById("ptv_region");
    var regSel = document.getElementById("ana_region");
    

    for (var a in plotObject) {
        generatorSel.options[generatorSel.options.length] = new Option(a, a);
    }
    generatorSel.onchange = function() {
      //empty Chapters- and Topics- dropdowns
      channelSel.length = 1;
      //display correct values 
      var b = plotObject[this.value];
      for (var i = 0; i < b.length; i++) {
          channelSel.options[channelSel.options.length] = new Option(b[i], b[i]);
      }
    }
    
    for (var i = 0; i < processObject.length; i++) {
        processSel.options[processSel.options.length] = new Option(processObject[i], processObject[i]);
    }
    for (var i = 0; i < njetObject.length; i++) {
        njetSel.options[njetSel.options.length] = new Option(njetObject[i], njetObject[i]);
    }
    for (var i = 0; i < ptvObject.length; i++) {
        ptvSel.options[ptvSel.options.length] = new Option(ptvObject[i], ptvObject[i]);
    }
    for (var i = 0; i < regObject.length; i++) {
        regSel.options[regSel.options.length] = new Option(regObject[i], regObject[i]);
    }
    
}

function all_plots() {


    var generatorSel = document.getElementById("generator").value;
    var channelSel = document.getElementById("channel").value;
    var processSel = document.getElementById("process").value;
    var njetSel = document.getElementById("jet_n").value;
    var ptvSel = document.getElementById("ptv_region").value;
    var regSel = document.getElementById("ana_region").value;
    
    //var generatorSel = "sherpa_221_vs_2211";
    //var channelSel = "0L";
    //var processSel = "Zbb";
    //var njetSel = "2btag2jet";
    //var ptvSel = "150_250";
    //var regSel = "SR";
    // var trainSel = "0L_TTbar"; var evalSel = "0L_TTbar_PP8"; var jetSel = "2J"; var regSel = "150_250"; var varSel = "mass";


    document.getElementById("mBB_plot").src="plots/"+generatorSel+"/"+channelSel+"/"+processSel+"/"+processSel+"_"+njetSel+"_"+ptvSel+"ptv"+"_"+regSel+"_mBB.png";
    document.getElementById("dRBB_plot").src="plots/"+generatorSel+"/"+channelSel+"/"+processSel+"/"+processSel+"_"+njetSel+"_"+ptvSel+"ptv"+"_"+regSel+"_dRBB.png";
    document.getElementById("pTV_plot").src="plots/"+generatorSel+"/"+channelSel+"/"+processSel+"/"+processSel+"_"+njetSel+"_"+ptvSel+"ptv"+"_"+regSel+"_pTV.png";
    document.getElementById("mva_plot").src="plots/"+generatorSel+"/"+channelSel+"/"+processSel+"/"+processSel+"_"+njetSel+"_"+ptvSel+"ptv"+"_"+regSel+"_mva.png";
}

function plots(imgs) {
    // Get the expanded image
    var expandImg = document.getElementById("expandedImg");
    // Get the image text
    var imgText = document.getElementById("imgtext");
    // Use the same src in the expanded image as the image being clicked on from the grid
    expandImg.src = imgs.src;
    // Use the value of the alt attribute of the clickable image as text inside the expanded image
    imgText.innerHTML = imgs.alt;
    // Show the container element (hidden with CSS)
    expandImg.parentElement.style.display = "block";
  }
